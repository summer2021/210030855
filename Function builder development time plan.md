# OpenFunction 的函数构建器开发时间规划

> 开源之夏-暑期2021 KubeSphere社区项目

## 最终目标

- 开发一个新的函数构建器（function builder）

## 可选目标

- 优化构建器策略
- 开发一套新的构建器框架

## 项目验收标准

- 构建器应可以完成从用户源代码到最终应用镜像的构建过程
- 构建器支持的函数语言应在已支持清单之外（已支持 go,java,python,node），可以为 PHP, Rust 等
- 构建器构建策略优化效果显著（可选）
- 自研构建器框架，且可以满足上述必选验收标准（可选）

## 项目开发计划

### 研发第一阶段

> 2021.7.1 - 2021.8.15

建议先熟悉 OpenFunction 项目，了解其工作流程。再学习 [Tekton](https://tekton.dev/) 与 [Cloud Native Buildpacks](https://buildpacks.io/) 项目，理解函数构建器的工作原理。

当前 OpenFunction 的函数构建器是基于 [GoogleCloudPlatform/buildpacks](https://github.com/GoogleCloudPlatform/buildpacks) 项目修改的，该项目也是基于 Cloud Native Buildpacks 原理的函数构建器项目。

当前 OpenFunction 的函数构建器项目地址：https://github.com/OpenFunction/builder

在对上述内容学习理解之后，便可以着手开发新的函数构建器。

预期本阶段的研发计划：

- 2021.7.1 - 2021.7.11，项目相关知识学习过程
- 2021.7.12 - 2021.8.8，实现一个 buildpacks demo
- 2021.8.9 - 2021.8.15，对第一阶段的研发内容进行整理

实际研发计划：

### 研发第二阶段

> 2021.8.16 - 2021.9.30

预期本阶段的研发计划：

- 2021.8.16 - 2021.8.27，设计并实现支持OpenFunction的buildpack
- 2021.8.28 - 2021.9.23，实现支持OpenFunction的函数构建器
- 2021.9.24 - 2021.9.30，对第二阶段的研发内容进行整理