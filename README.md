[TOC]

## 项目信息

### 项目名称

OpenFunction 的函数构建器开发

### 方案描述

本项目 *OpenFunction 的函数构建器* 开发基于 Cloud Native Buildpacks (以下简称 CNB ) 原理。CNB 是云原生的应用镜像构建工具，它主要的作用是将应用源代码打包成符合 OCI 标准的镜像，供给各种云环境使用。

CNB 可以分为几个组成部分：Builder、Buildpack、Lifecycle、Stack。其中本项目 *OpenFunction 的函数构建器开发*，就是要根据 CNB 原理开发自定义函数构建器 builder，使其可以识别 Ruby 语言，并将函数构建成可以运行在 OpenFunction 中的应用镜像。

#### Builder

Builder是一个镜像，其中包含执行构建所需的所有组件。Builder 镜像是通过获取构建镜像并添加生命周期、构建包和文件来创建的，这些文件配置构建的各个方面，包括构建包检测顺序和运行镜像的位置。

#### Lifecycle

Lifecycle 是 CNB 最重要的组件，它将由应用源代码到镜像的构建步骤抽象出来，完成了对整个过程的编排，并最终产出应用镜像。

它所编排的步骤为：

-  Detect ，判断是否可以使用该 buildpack 。每一个 detector 在构建环境中被调用，没有参数传入，有一个输入文件 order.toml 和两个输出文件 group.toml、plan.toml。order.toml 配置文件里包含很多分类组，每个组里又包含许多 buildpacks 。Detector 检查每一各组，如果有选中的 buildpack 则将相关结果及依赖分别输出到 group.toml、plan.toml 配置文件。
- Analyze ，与 Restore 配合，为 Build 阶段提供文件、 layers 的缓存。需要在上一个 Build 阶段定义需要缓存的 layer。 
- Restore ，与 Analyze 配合，为 Build 阶段提供文件、 layers 的缓存。需要在上一个 Build 阶段定义需要缓存的 layer 。
- Build ，将源代码构建为容器中的可运行程序。
-  Export ，输出OCI镜像
- Create ， 一键执行上述5个步骤（按顺序）。
- Launch ，负责启动镜像中的应用。
- Rebase ，用于替换应用的 run 镜像。

#### Buildpack

Buildpack 可以理解为任务单一的构建单元，用于检查您的应用程序源代码并制定构建和运行应用程序的计划。每一个 buildpack 都具备以下三个元素：

- buildpack.toml ，描述了 buildpack 的元数据。
- bin/detect ，Lifecycle 中 Detect 阶段的实现，确定是否应用该 buildpack。
- bin/build ，Lifecycle 中 Build 阶段的实现，执行buildpack逻辑。

它的工作原理可以简单理解为：bin/detect 将会判断是否可以进入该 buildpack ，如果可以进入该 buildpack ，那么 Lifecycle 就会指导流程进入 Build 阶段同时触发 bin/build 执行。

每个 buildpack 完成后都会产出一份清单，用于表明自己产出了什么内容（可为空），且如果要完成后续的 buildpack 过程，还需要什么内容（可为空）。 

同时在 Build 阶段，基于复用构建逻辑的原则，buildpack 的作者可以设置不通过的 layer （layer 内保存了所关联的构建过程的结果，如文件、运行时等内容），并设置 build 、launch 、cache 三种属性。Lifecycle 将根据这三个参数的组合来决定是否复用一个 layer。 

#### Stack

Stack 的功能是提供基础运行环境，它包含两种镜像：

- Build镜像，为 Lifecycle 的 Build 阶段提供运行环境
- Run镜像 ，为业务应用提供运行环境

在 OpenFunction 中，CNB主要用于 build 阶段，配合 Tekton 完成函数从源代码到镜像的制作。制作过程由 buildpacks.yaml 描述，分为三个步骤：

- prepare ，准备环境变量。
- create ，使用 /cnb/lifecycle/creator（即 Lifecycle 中的 Create 阶段的实现）进行源代码的构建。
- results ，输出镜像的摘要。

OpenFunction 还需要基于 Buildpacks 的原理制作适用于 serverless（FaaS）场景的 builder 。

这个 builder 需要具备以下特性：

- 封装函数源代码，使之能被多种常见的事件源触发（function framework）
- 快速构建镜像
- 适用于在线或离线构建场景

本次项目的总目标就是构建 CNB 中的组件 Builder，使 OpenFunction 可以支持更多的主流语言（及更多的语言版本）,如果时间充裕的话，还可以优化代码构建的策略、减少代码构建的时间。

目前，GoogleCloudPlatform/buildpacks 项目官方 builders 做的已经很好了，且支持Go、Java、Php、Node.js、Python 等多种语言，多种版本。但如果我们想适用于自己的项目 OpenFuncton，则需要我们自己重构Framework，然后将 Framework 集成到 builder 里。

项目整体方案就是基于 GoogleCloudPlatform/buildpacks 项目和 Cloud Native Buildpacks 原理实现自定义语言builder

### 时间规划

#### 研发第一阶段（2021.7.1 - 2021.8.15）

先熟悉 OpenFunction 项目，了解其工作流程。再学习 Tekton 与 Cloud Native Buildpacks 项目，理解函数构建器的工作原理。

当前 OpenFunction 的函数构建器是基于 GoogleCloudPlatform / buildpacks 项目修改的，该项目也是基于 Cloud Native Buildpacks 原理的函数构建器项目。

在对上述内容学习理解之后，便可以着手开发新的函数构建器。

预期本阶段的研发计划：

- 2021.7.1 - 2021.7.11，项目相关知识学习过程
- 2021.7.12 - 2021.8.8，实现 ruby builder
- 2021.8.9 - 2021.8.15，对第一阶段的研发内容进行整理

#### 研发第二阶段（2021.8.16 - 2021.9.30）

经过研发第一阶段，相信个人已经掌握了开源项目的协作技能及开发方式，下一阶段将集成社区人员研发的Nodejs 函数框架 functions-framework-nodejs 到 Nodejs builder 中，使 Nodejs builder可以构建适用于 OpenFuncton 的 Nodejs 函数

- 2021.8.16 - 2021.8.27，学习 functions-framework-nodejs 函数框架实现机理
- 2021.8.28 - 2021.9.23，设计并实现集成 Nodejs 函数框架的 builder
- 2021.9.24 - 2021.9.30，对第二阶段的研发内容进行整理


## 项目总结

### 项目产出

- Ruby Stack 构建完成，支持 Ruby 2.6 运行时

  根据 CNB 原理，完成了 Ruby builder 的 Stack 构建，该 Stack  提供 Ruby 2.6 运行时。

-  实现了 Ruby builder 构建，使其可以支持 HTTP 等类型的 ruby 函数

  根据 CNB 原理，设计并实现了各种 buildpack  完成了 Ruby builder 镜像的生成，使用该 builder 可以完成 Ruby 函数从源码到镜像的构造。

- 在 nodejs builder 中添加一层 buildpack 使 nodejs 函数框架集成到 builder

  设计并完成了在 nodejs builder  增加一层 builderpack , 在 builder 中集成开发好的 functions-framework-nodejs 函数框架。

- 使用 nodejs builder 可以正确构建函数

  完成，可以使用 nodejs builder  来处理各种类型的 nodejs 函数。

- 添加补充了一些项目文档和 bulder 测试用例

  在项目开发过程中，记录了一些项目文档，并且在本文章 demo 小节还演示了一些测试用例函数。

### 方案进度

根据项目开发计划及项目开发方案，按部就班的向前推进项目，并已经完成项目的开发。

**遇到的问题及解决方案**

在本项目开发过程中，遇到了各类问题，现在把问题及解决方案汇总如下：

Q1:在搭建项目环境时，遇到各种依赖工具下载版本及网络问题

A1:主要是之前没有linux下的项目开发经验，对Linux熟悉度也不够，才会造成这一系列问题，网络问题主要是一些谷歌资源镜像网址问题。

Q2:在stack运行时构建完毕，开始与buildpack合成builder时，发现报错内容为ERROR: failed to add buildpacks to builder: downloading buildpack: extracting from registry 'bundle.tgz': fetching image: image 'bundle.tgz' does not exist on the daemon: not found。

A2：主要是不熟悉bazel工具，忽略了路径问题， bazel 工具将相关buidpack 代码目录打包成了tgz文件,而工程里在关联这些buildpack 包时没有设置正确的解析路径。

Q3:前期以为ruby2.6运行时在stack里已经安装好了，builder也生成了，但builder却无法使用，报各类错误，举一处报错内容为[builder] /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require': cannot load such file -- zlib (LoadError)

A3：经过排查，发现ruby2.6运行时在 stack里没有安装完整，ruby编译安装时，已经给过提示说openssl、zlib等是没有被编译的，缺少依赖。于是重新下载这些包，但发现ubuntu和centos包还不一样，一经波折才处理好这些连锁问题。

Q4:当测试HTTP类型函数时，竟然奇妙的报错，说在run 环境下没有找到bundler对应版本，报错内容为 `find_spec_for_exe': Could not find 'bundler' (2.2.24) required by your /workspace/Gemfile.lock. (Gem::GemNotFoundException)

A4：一波排查之后，发现原来是build 和 run 镜像里存在多个bundler版本，直接将默认版本1.17.2卸载，重新下载一致版本就解决了。

### 项目完成质量

这是我第一次参加社区开源项目，在开发项目的过程中，学到了很多。从最开始对项目一无所知，到上手开发项目，并最终完成项目。一次很难得的学习经历。项目完成质量就我个人而言，只是完成了项目本身而已，还有许多需要优化及扩展的地方。

### 与导师沟通及反馈情况

OpenFunction 的函数构建器开发这个项目是我第一次深入接触开源社区并参与社区贡献，社区的大牛们都很积极，不仅提供服务器资源，更是深入指导，督促学生，每周一次的线上会议，让大家积极汇报自己的工作进展。刚开始接触项目时，完全不懂，复现环境都是一件困难的事情,版本问题、网络问题等，甚至是开发环境都不熟悉。

前期与导师交流了多次之后，才慢慢了解项目要做什么，怎么做，大方向有了，但具体细节还要自己下功夫，在项目中，慢慢学会了 linux 环境下开发项目经验、bazel 的简单使用及其功能、docker 的学习与使用、ruby 语言的学习等等。

感觉学习了很久才慢慢真正的步入正轨，开始推进项目，但之前没有过开源项目开发经验，GitHub 使用也不是那么熟练，project 看板功能没有使用过，也没有尝试过 Issue 驱动式开发经历等等，但在这次历练中，学会了很多，提了 Issue，提了 pull request ,并对 request changed 也不陌生了，是一次难得的宝贵开源项目开发经验。 

感谢在整个项目开发过程中导师的耐心讲解与解惑。

## 项目设计与实现

### 项目设计

OpenFunction 是一个开源的函数即服务（FaaS）框架，和大多数同类产品一样，旨在让用户专注于他们的业务逻辑，而不必担心底层运行环境和基础设施。如下图所示函数生命周期中几个重要的部分分别是: 函数框架（Functions framework）、函数构建 (Build）、函数服务 （Serving）和事件驱动框架 （Events Framework）

![img](https://static001.geekbang.org/infoq/27/2740c2a1626e4a595aca01c6da293080.png)

​                                                                        OpenFunction  函数生命周期

![img](https://static001.geekbang.org/infoq/bc/bc7da8cec292cffdee570af760e3a181.png)

​                                                                       OpenFunction  组件示意图

本项目所做的工作就是 OpenFunction 组件 Build 部分

#### 函数构建（Build）

在 OpenFunction  通常会用 Build 来指代容器镜像的打包，但实际上将源代码打包成镜像只是构建工作中的一个步骤，开发者还有诸如拉取代码、代码预处理、镜像上传等工作需要完成。由此将 Build 拆分为两个主要的功能点，即制作容器镜像与创建构建流水线。在社区人员调研了 Cloud Native Buildpacks（CNB）、Tekton、Shipwright 等开源项目后，OpenFunction 项目最终设计了 OpenFunction Builder CRD，为用户提供了一种可以自由选择 Build 方案的 Build 框架。

Docker 被 Kubernetes 放弃作为默认的容器运行时后，开发者在 Kubernetes 中制作容器镜像还有多种选择比如 Kaniko、Buildah、BuildKit 以及 Cloud Native Buildpacks（CNB）。其中前三者均依赖 Dockerfile 去制作容器镜像，而 Cloud Native Buildpacks（CNB）是云原生领域最新涌现出来的新技术，它不依赖于 Dockerfile，而是能自动检测要 build 的代码，并生成符合 OCI 标准的容器镜像，已经被 Google Cloud、IBM Cloud、Heroku、Pivotal 等公司采用。

OpenFunction 选择 Cloud Native Buildpacks（CNB） 作为容器镜像制作的默认选择，陆续也会支持 Kaniko、Buildah、BuildKit 等方式。

Cloud Native Buildpacks（CNB） 的核心是 CNB Lifecycle，它负责将由应用源代码到镜像的构建步骤抽象出来，形成一套标准规范从而完成对整个过程的编排，并最终产出应用镜像。这样一来，开发者就可以将不同逻辑的最小构建单元 buildpack（可以理解为 Dockerfile 中的镜像分层） 按自身的需求组合到一起，生成一个构建器（builder），再交由 CNB 处理镜像的构建过程。

因为这是一套开源的标准，所以在 OpenFunction Builder 中开发者不但可以选择 OpenFunction 自身的构建器（builder）来构建镜像，还可以选择任何一种符合 CNB Lifecycle 的构建器，如 Google buildpacks、Paketo buildpacks 等，这意味着使用者可以构建任何语言、类型的应用。

Build 的另一个需求——构建流水线，就需要借助 Tekton 这样优秀的流水线工具作为支持。在最开始的版本中，OpenFunction 毫不犹豫地选择 Tekton 来拆分构建环节的工作，为之前所谈到的构建任务在 Tekton 中创建对应的 Task 及 Pipeline 等。

在对 Shipwright 的调研中社区发现 Shipwright 同样由 Tekton 驱动，并且 Shipwright 将 Tekton 的设计理念带入了镜像构建过程，形成了非常云原生的镜像构建框架，同时也支持使用 Kaniko、Buildah、BuildKit 以及 Cloud Native Buildpacks（CNB）构建镜像，并可以通过指定 BuildStrategy 和 ClusterBuildStrategy 在上述四种镜像构建方法之间进行切换。

在 OpenFunction v0.3.0 版本中将原有的 Tekton + Cloud Native Buildpacks 的构建方案切换成了 Shipwright。OpenFunction Builder 从设计上完美解决了如何在没有 Dockerfile 的情况下制作容器镜像的问题，并且具备了高度自由、云原生的构建器（构建方案）选择机制。

> 引用 https://www.infoq.cn/article/aqZsasOwiFAYwvGMkU9O

### 项目实现

#### 本地环境

- Ubuntu 18.04.4   user:ubuntu
- CPU Core * 8
- Memory * 8G
- Goland 2020.3

#### 依赖工具

整个开发过程需要需要 container-structure-test、Bazel、pack、Docker 这些工具，确保这些工具的执行文件处于 $PATH 目录下。

- 安装 Docker

  ```shell
  sudo apt install docker.io
  sudo usermod -aG docker ubuntu
  sudo chmod a+rw /var/run/docker.sock
  sudo systemctl restart docker 
  ```

- 安装pack

  ```shell
  curl -sSL "https://github.com/buildpacks/pack/releases/download/v0.18.1/pack-v0.18.1-linux.tgz" | sudo tar -C /usr/local/bin/ --no-same-owner -xzv pack
  ```

- 安装Bazel (v4.1.0)

  ```shell
  sudo apt install g++ unzip zip
  sudo apt-get install openjdk-11-jdk
  wget https://github.com/bazelbuild/bazel/releases/download/4.1.0/bazel-4.1.0-installer-linux-x86_64.sh
  chmod +x bazel-4.1.0-installer-linux-x86_64.sh
  ./bazel-4.1.0-installer-linux-x86_64.sh --user
  sudo mv $HOME/bin/bazel /usr/local/bin
  ```

- 安装 container-structure-test

  ```shell
  curl -LO https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64 && chmod +x container-structure-test-linux-amd64 && sudo mv container-structure-test-linux-amd64 /usr/local/bin/container-structure-test
  ```


使用以下命令验证是否已准备好所有依赖：

```shell
git clone https://github.com/OpenFunction/builder.git
cd builder
bazel test --test_output=errors tools/checktools:main_test
```

如果出现类  INFO: Build completed successfully, 9 total actions 提示信息，说明已经所有依赖安装完成

#### Goland 配置远程服务器

连接远程服务器，并配置文件同步路径

![image-20210814111538411](https://cdn.jsdelivr.net/gh/penghuima/ImageBed@master/img/blog_file/PicGo-Github-ImgBedimage-20210814111538411.png)

![image-20210814111619937](https://cdn.jsdelivr.net/gh/penghuima/ImageBed@master/img/blog_file/PicGo-Github-ImgBedimage-20210814111619937.png)

> [Create a remote server configuration](https://www.jetbrains.com/help/go/creating-a-remote-server-configuration.html)
>
> [Goland同步插件配置](https://blog.csdn.net/huwh_/article/details/77879152?utm_source=blogxgwz3)

#### Build ruby26 stack

##### stack 概述

Stack 的功能是提供基础运行环境，它包含两种镜像：

- build ，为 Lifecycle 的 Build 阶段提供运行环境

- run ，为业务应用提供运行环境

在本项目Openfunction的Ruby函数构建器开发中，需要先准备一个部署有Ruby2.6 runtime 的 stack ，stack 本质为一个基础运行环境，在制作过程中加入 LABEL io.buildpacks.stack.id 即可以指定 stack 的标识。

> 原生 gcp/buildpacks 将下载语言 runtime 的步骤放置在 buildpack 中，即在探测到语言类型后再下载安装对应的语言 runtime ，本项目选择将语言 runtime 安装在 stack 中，以减少函数构建过程中的时间

##### ruby 2.6 runtime

本项目自定义的 ruby2.6 stack ：

```shell
FROM gcr.io/gcp-runtimes/ubuntu_18_0_4
ARG cnb_uid=1000
ARG cnb_gid=1000
ARG stack_id="openfunction.ruby26"
# Required by python/runtime: libexpat1, libffi6, libmpdecc2.
# Required by dotnet/runtime: libicu60
# Required by go/runtime: tzdata (Go may panic without /usr/share/zoneinfo)
RUN apt-get update && apt-get install -y --no-install-recommends \
  libexpat1 \
  libffi6 \
  libmpdec2 \
  libicu60 \
  libc++1-9 \
  tzdata \
  gcc libc6-dev make  zlib1g zlib1g-dev  openssl libssl-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

LABEL io.buildpacks.stack.id=${stack_id}

RUN groupadd cnb --gid ${cnb_gid} && \
  useradd --uid ${cnb_uid} --gid ${cnb_gid} -m -s /bin/bash cnb

# install ruby runtime
RUN cd /tmp && mkdir ruby && cd ruby && curl  https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.6.tar.gz | tar xz \
    && cd ruby-2.6.6 && ./configure && make && make install  \
    && cd ext/zlib \
    && ruby extconf.rb && sed -i 's?$(top_srcdir)?../..?g' Makefile  && make && make install \
    && cd ../openssl \
    && ruby extconf.rb && sed -i 's?$(top_srcdir)?../..?g' Makefile && make && make install \
    && gem uninstall bundle && gem install bundler -v 2.1.4 \
    && cd ../../

ENV CNB_USER_ID=${cnb_uid}
ENV CNB_GROUP_ID=${cnb_gid}
ENV CNB_STACK_ID=${stack_id}
```

> 完整文件请参考 [ruby26/stack](ruby26/stack)

##### 生成 stack

开始在根目录 builder/ 下执行 ruby2.6 stack 构建命令

```shell
bazel run //builders/ruby26/stack:build
```

This command creates three images:

- **ruby26common**
- **openfunctiondev/buildpacks-ruby26-run:v1**
- **openfunctiondev/buildpacks-ruby26-build:v1**

如下所示

```shell
ubuntu@i-a1kwg7lk:~/mph/builder$ docker images
REPOSITORY                                TAG       IMAGE ID       CREATED         SIZE
openfunctiondev/buildpacks-ruby26-build   v1        18a6f7e4c4b6   3 minutes ago   1.04GB
openfunctiondev/buildpacks-ruby26-run     v1        e221e925ecc5   4 minutes ago   791MB
ruby26common                              latest    c21fa60b9e9c   4 minutes ago   791MB
gcr.io/gcp-runtimes/ubuntu_18_0_4         latest    93a9bd0cfd2e   51 years ago    76.7MB
```

验证 ruby 2.6 runtime

```shell
ubuntu@i-a1kwg7lk:~/mph/builder$ docker run -it --name build-runtime openfunctiondev/buildpacks-ruby26-build:v1 /bin/bash
cnb@67882f6e62ab:/$ which ruby
/usr/local/bin/ruby
cnb@67882f6e62ab:/$ bundle -v
Bundler version 2.1.4

ubuntu@i-a1kwg7lk:~/mph/builder$ docker run -it --name run-runtime openfunctiondev/buildpacks-ruby26-run:v1 /bin/bash
cnb@704a89adc952:/$ which ruby
/usr/local/bin/ruby
cnb@704a89adc952:/$ bundle -v
Bundler version 2.1.4
```

##### 上传镜像 

可以将 build 和 run 镜像上传到自己的镜像仓库

docker login -u xxxxx(dockerhub注册的docker用户名)，登录docker成功

```shell
docker login -u penghuima
docker tag openfunctiondev/buildpacks-ruby26-build:v1 penghuima/buildpacks-ruby26-build:v1
docker tag openfunctiondev/buildpacks-ruby26-run:v1 penghuima/buildpacks-ruby26-run:v1
docker push penghuima/buildpacks-ruby26-build:v1
docker push penghuima/buildpacks-ruby26-run:v1
```

#### 生成builder

##### builder概述

Builder 描述了一个完整的构建编排逻辑。它本质上是将  Stack 及若干个 Buildpack 组合在一起，形成一个业务逻辑明确的构建工具。Builder是一个镜像，其中包含执行构建所需的所有组件。构建器镜像是通过stack镜像并添加生命周期、构建包和文件来创建的，这些文件配置构建的各个方面，包括构建包检测顺序和运行镜像的位置。

![create-builder diagram](https://buildpacks.io/docs/concepts/components/create-builder.svg)本项目builder的定义文件 builder.toml内容如下

```shell
description = "Builder for the  Ruby 2.6"

[[buildpacks]]
  id = "google.ruby.functions-framework"
  uri = "ruby/functions_framework.tgz"

[[buildpacks]]
  id = "google.ruby.bundle"
  uri = "ruby/bundle.tgz"

[[buildpacks]]
  id = "google.config.entrypoint"
  uri = "entrypoint.tgz"

[[buildpacks]]
  id = "google.utils.label"
  uri = "label.tgz"

[[order]]

  [[order.group]]
    id = "google.ruby.bundle"

  [[order.group]]
    id = "google.ruby.functions-framework"

  [[buildpacks]]
    id = "google.config.entrypoint"

  [[order.group]]
    id = "google.utils.label"

[stack]
  id = "openfunction.ruby26"
  build-image = "openfunctiondev/buildpacks-ruby26-build:v1"
  run-image = "openfunctiondev/buildpacks-ruby26-run:v1"

[lifecycle]
  version = "0.11.1"
```

- [[buildpacks]] 表示引入的 buildpack ，已经用 bazel 工具将相关代码目录打包成了 tgz 文件，就是封装了 /bin/detect 和 /bin/build 两个功能。
- [[order]]|[[order.group]] 表示 buildpack 的执行顺序，从下文 of/ruby26镜像构成的 app镜像运行日志中，可以看到运行检测顺序与 [[order]] 顺序一致
- [stack] 表示引用的 stack 内容，其中stack id 唯一标识我们上文中创建的 stack
- [lifecycle] 表示使用的 lifecycle 模块的版本

##### buildpacks

Buildpack 可以理解为任务单一的构建单元，用于检查您的应用程序源代码并制定构建和运行应用程序的计划。每一个 buildpack 都具备以下三个元素：

- buildpack.toml ，描述了 buildpack 的元数据。
- bin/detect ，Lifecycle 中 Detect 阶段的实现，确定是否应用该buildpack。
- bin/build ，Lifecycle 中 Build 阶段的实现，执行buildpack逻辑。

它的工作原理可以简单理解为：bin/detect 将会判断是否可以进入该 buildpack ，如果可以进入该 buildpack ，那么 Lifecycle 就会指导流程进入 Build 阶段同时触发 bin/build 执行。

每个 buildpack 完成后都会产出一份清单，用于表明自己产出了什么内容（可为空），且如果要完成后续的 buildpack 过程，还需要什么内容（可为空）。 

同时在 Build 阶段，基于复用构建逻辑的原则，buildpack 的作者可以设置不通过的 layer （layer 内保存了所关联的构建过程的结果，如文件、运行时等内容），并设置 build 、launch 、cache 三种属性。Lifecycle 将根据这三个参数的组合来决定是否复用一个 layer。 

以 id 为  "google.ruby.functions-framework" 的 ruby 函数框架这一 buildpack 为例，其元数据在 [buildpack] 中定义，[[stacks]] 用于描述 buildpack 可以运行在哪些 stack 上

```shell
api = "0.2"

[buildpack]
id = "google.ruby.functions-framework"
version = "0.9.1"
name = "Ruby - Functions Framework"

[[stacks]]
id = "openfunction.ruby26"

[[stacks]]
id = "google.ruby27"
```

##### 生成 builder

开始在根目录 builder/ 下执行 ruby2.6 builder 构建命令

```shell
bazel build //builders/ruby26:builder.image
```

This command creates one image:

- **of/ruby26**

生成 builder 日志

```sh
ubuntu@i-a1kwg7lk:~/mph/builder$ bazel build //builders/ruby26:builder.image
INFO: Analyzed target //builders/ruby26:builder.image (54 packages loaded, 6870 targets configured).
INFO: Found 1 target...
INFO: From Executing genrule //builders/ruby26:builder.image:
2021/08/14 14:58:20 Checking tools
2021/08/14 14:58:20 pack found at /usr/local/bin/pack
2021/08/14 14:58:20 docker found at /usr/bin/docker
2021/08/14 14:58:20 container-structure-test found at /usr/local/bin/container-structure-test
2021/08/14 14:58:20 Checking pack version
2021/08/14 14:58:20 Found pack version 0.18.1+git-b5c1a96.build-2373
Extracting builder tar:
./
./builder.toml
./label.tgz
./ruby/
./ruby/functions_framework.tgz
./ruby/bundle.tgz
Warning: Command 'pack create-builder' has been deprecated, please use 'pack builder create' instead
Downloading from 'https://github.com/buildpacks/lifecycle/releases/download/v0.11.1/lifecycle-v0.11.1+linux.x86-64.tgz'
5.25 MB/5.25 MB
Successfully created builder image 'of/ruby26'
Tip: Run 'pack build <image-name> --builder of/ruby26' to use this builder
Target //builders/ruby26:builder.image up-to-date:
  bazel-bin/builders/ruby26/builder.sha
INFO: Elapsed time: 14.209s, Critical Path: 12.22s
INFO: 48 processes: 19 internal, 28 linux-sandbox, 1 local.
INFO: Build completed successfully, 48 total actions
```

#### Ruby builder Demo 展示

本小节通过展示一个简单的  hello  word 例子来演示 builder 是如何工作的

> git clone https://github.com/penghuima/samples.git    
>
> cd  samples/functions/Knative/hello-world-ruby/

sample里主要有三个文件 app.rb、Gemfile、Gemfile.lock 内容如下

- app.rb

  ```ruby
  require "functions_framework"
  FunctionsFramework.http("Hello-World") do |request|
    "Hello, world!  Hello Ruby!\n"
  end
  ```

- Gemfile

  ```ruby
  source "https://rubygems.org"
  gem "functions_framework", "~> 1.0"
  ```

- Gemfile.lock

  ```ruby
  GEM
    remote: https://rubygems.org/
    specs:
      cloud_events (0.5.1)
      functions_framework (1.0.0)
        cloud_events (>= 0.5.1, < 2.a)
        puma (>= 4.3.0, < 6.a)
        rack (~> 2.1)
      nio4r (2.5.7)
      puma (5.3.2)
        nio4r (~> 2.0)
      rack (2.2.3)
  
  PLATFORMS
    ruby
  
  DEPENDENCIES
    functions_framework (~> 1.0)
  
  BUNDLED WITH
     2.1.4
  ```

##### 生成 app image

原理如下图所示，由 builder镜像和源码文件 生成 app images

![build diagram](https://buildpacks.io/docs/concepts/operations/build.svg)

Download samples:

```shell
git clone https://github.com/penghuima/samples.git    
cd  samples/functions/Knative/hello-world-ruby/
```

Build the function:

```shell
pack build function-ruby --builder of/ruby26 --env FUNC_NAME="Hello-World"
docker run --rm -p8080:8080 function-ruby
```

Visit the function:

```shell
curl http://localhost:8080
```

Output example:

```shell
Hello, world!  Hello Ruby!
```

##### 运行日志

```shell
ubuntu@i-a1kwg7lk:~/mph/samples/functions/Knative/hello-world-ruby$ pack build function-ruby --builder of/ruby26 --env FUNC_NAME="Hello-World"
v1: Pulling from openfunctiondev/buildpacks-ruby26-run
Digest: sha256:f428f2272085771fd21353d3c3ba58ce1e755f47608dc11e2bd19d5f85df4066
Status: Image is up to date for openfunctiondev/buildpacks-ruby26-run:v1
0.11.1: Pulling from buildpacksio/lifecycle
Digest: sha256:2ca9870d9472400792bdc2d43ded8df2642ffcf937e8cd01685836678c28780b
Status: Image is up to date for buildpacksio/lifecycle:0.11.1
===> DETECTING
[detector] google.ruby.bundle              0.9.0
[detector] google.ruby.functions-framework 0.9.1
[detector] google.utils.label              0.0.1
===> ANALYZING
[analyzer] Previous image with name "function-ruby" not found
===> RESTORING
===> BUILDING
[builder] === Ruby - Bundle (google.ruby.bundle@0.9.0) ===
[builder] Installing application dependencies.
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local without development test"
[builder] Done "bundle config --local without development test" (213.701121ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local path .bundle/gems"
[builder] Done "bundle config --local path .bundle/gems" (204.372547ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle lock --add-platform x86_64-linux"
[builder] Fetching gem metadata from https://rubygems.org/...
[builder] Resolving dependencies...
[builder] Writing lockfile to /workspace/Gemfile.lock
[builder] Done "bundle lock --add-platform x86_64-linux" (4.131479657s)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle lock --add-platform ruby"
[builder] Fetching gem metadata from https://rubygems.org/..
[builder] Writing lockfile to /workspace/Gemfile.lock
[builder] Done "bundle lock --add-platform ruby" (1.109658296s)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local deployment true"
[builder] Done "bundle config --local deployment true" (206.397619ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local frozen true"
[builder] Done "bundle config --local frozen true" (192.510552ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local without development test"
[builder] Done "bundle config --local without development test" (214.752127ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle config --local path .bundle/gems"
[builder] Done "bundle config --local path .bundle/gems" (218.378794ms)
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle install (NOKOGIRI_USE_SYSTEM_LIBRARIES=1 MALLOC_ARENA_MAX=2 LANG=C.utf8)"
[builder] Fetching gem metadata from https://rubygems.org/..
[builder] Using bundler 2.1.4
[builder] Fetching cloud_events 0.5.1
[builder] Installing cloud_events 0.5.1
[builder] Fetching nio4r 2.5.7
[builder] Installing nio4r 2.5.7 with native extensions
[builder] Fetching puma 5.3.2
[builder] Installing puma 5.3.2 with native extensions
[builder] Fetching rack 2.2.3
[builder] Installing rack 2.2.3
[builder] Fetching functions_framework 1.0.0
[builder] Installing functions_framework 1.0.0
[builder] Bundle complete! 1 Gemfile dependency, 6 gems now installed.
[builder] Gems in the groups development and test were not installed.
[builder] Bundled gems are installed into `./.bundle/gems`
[builder] Done "bundle install (NOKOGIRI_USE_SYSTEM_LIBRARIES=1 MALLOC_ARENA..." (8.561774732s)
[builder] === Ruby - Functions Framework (google.ruby.functions-framework@0.9.1) ===
[builder] --------------------------------------------------------------------------------
[builder] Running "bundle exec functions-framework-ruby --quiet --verify --source app.rb --target Hello-World (MALLOC_ARENA_MAX=2 LANG=C.utf8 RACK_ENV=production)"
[builder] OK
[builder] Done "bundle exec functions-framework-ruby --quiet --verify --sour..." (319.197753ms)
[builder] === Utils - Label Image (google.utils.label@0.0.1) ===
===> EXPORTING
[exporter] Adding layer 'google.ruby.bundle:gems'
[exporter] Adding layer 'google.ruby.functions-framework:functions-framework'
[exporter] Adding 1/1 app layer(s)
[exporter] Adding layer 'launcher'
[exporter] Adding layer 'config'
[exporter] Adding layer 'process-types'
[exporter] Adding label 'io.buildpacks.lifecycle.metadata'
[exporter] Adding label 'io.buildpacks.build.metadata'
[exporter] Adding label 'io.buildpacks.project.metadata'
[exporter] Setting default process type 'web'
[exporter] Saving function-ruby...
[exporter] *** Images (d46c60a6b4b8):
[exporter]       function-ruby
[exporter] Adding cache layer 'google.ruby.bundle:gems'
Successfully built image function-ruby
ubuntu@i-a1kwg7lk:~/mph/samples/functions/Knative/hello-world-ruby$ docker images
REPOSITORY                                TAG       IMAGE ID       CREATED        SIZE
openfunctiondev/buildpacks-ruby26-build   v1        18a6f7e4c4b6   6 hours ago    1.04GB
openfunctiondev/buildpacks-ruby26-run     v1        e221e925ecc5   6 hours ago    791MB
buildpacksio/lifecycle                    0.11.1    67c0e8a0dbed   41 years ago   15.7MB
of/ruby26                                 latest    19d1b18882a7   41 years ago   1.07GB
function-ruby                             latest    d46c60a6b4b8   41 years ago   801MB
ubuntu@i-a1kwg7lk:~/mph/samples/functions/Knative/hello-world-ruby$ docker run --rm -p8080:8080 function-ruby
I, [2021-08-14T09:41:20.648220 #1]  INFO -- : FunctionsFramework v1.0.0
I, [2021-08-14T09:41:20.648325 #1]  INFO -- : FunctionsFramework: Loading functions from "./app.rb"...
I, [2021-08-14T09:41:20.648584 #1]  INFO -- : FunctionsFramework: Looking for function name "Hello-World"...
I, [2021-08-14T09:41:20.648653 #1]  INFO -- : FunctionsFramework: Starting server...
I, [2021-08-14T09:41:20.715425 #1]  INFO -- : FunctionsFramework: Serving function "Hello-World" on port 8080...
ubuntu@i-a1kwg7lk:~$ curl http://localhost:8080
Hello, world!  Hello Ruby!
```

#### Nodejs builder Demo 展示

functions-framework-nodejs 函数框架支持三种 Function source type，分别为 http、cloudevent、openfunction

##### http

在本小节中展示 http 函数类型 demo

本 demo 中主要有三个文件，分别为 index.js 、package.json 、config.json

- index.js

  ```sh
  exports.helloWorld = (req, res) => {
    return('Hello, World!\n');
  };
  ```

- package.json

  ```sh
  {
    "dependencies": {
      "@openfunction/functions-framework": "0.3.0"
    },
    "scripts": {
      "start": "functions-framework --target=helloWorld --source http "
    }
  }
  ```

###### 生成 app image

使用 builder of/node14 将用户源码构建为镜像 node-http

```sh
pack build node-http  --builder of/node14
```

###### 起容器测试

以镜像 node-http 起容器测试

```sh
docker run --rm -p8080:8080 node-http
```

打开另一个终端访问 ，可看到输出结果

```sh
ubuntu@i-a1kwg7lk:~$ curl localhost:8080
{"Hello, World!\n"}
```

##### cloudevent

在本小节中展示 cloudevent 函数类型 demo

本 demo 中主要有三个文件，分别为 index.js 、package.json 、config.json

- index.js

  ```sh
  exports.helloCloudEvents = (cloudevent) => {
    return cloudevent
  }
  ```

- package.json

  ```sh
  {
    "dependencies": {
      "@openfunction/functions-framework": "0.3.0"
    },
    "scripts": {
      "start": "functions-framework --target=helloCloudEvents --source=cloudevent"
    }
  }
  ```

###### 生成 app image

使用 builder of/node14 将用户源码构建为镜像 node-cloudevent

```sh
pack build node-cloudevent  --builder of/node14
```

###### 起容器测试

以镜像 node-cloudevent 起容器测试

```sh
docker run --rm -p8080:8080 node-cloudevent
```

打开另一个终端访问 ，可看到输出结果

输入测试1

```sh
curl -X POST \
     -d'@./mock/payload/binary.json' \
     -H'Content-Type:application/json' \
     -H'ce-specversion:1.0' \
     -H'ce-type:com.github.pull.create' \
     -H'ce-source:https://github.com/cloudevents/spec/pull/123' \
     -H'ce-id:45c83279-c8a1-4db6-a703-b3768db93887' \
     -H'ce-time:2019-11-06T11:17:00Z' \
     -H'ce-myextension:extension value' \
     http://localhost:8080/
```

返回结果1

```sh
ubuntu@i-a1kwg7lk:~/functions-framework-nodejs$ curl -X POST \
>      -d'@./mock/payload/binary.json' \
>      -H'Content-Type:application/json' \
>      -H'ce-specversion:1.0' \
>      -H'ce-type:com.github.pull.create' \
>      -H'ce-source:https://github.com/cloudevents/spec/pull/123' \
>      -H'ce-id:45c83279-c8a1-4db6-a703-b3768db93887' \
>      -H'ce-time:2019-11-06T11:17:00Z' \
>      -H'ce-myextension:extension value' \
>      http://localhost:8080/
{
	"datacontenttype":"application/json",
	"data":{
		"runtime":"cloudevent"
	 },
	 "specversion":"1.0",
	 "type":"com.github.pull.create",
	 "source":"https://github.com/cloudevents/spec/pull/123",
	 "id":"45c83279-c8a1-4db6-a703-b3768db93887",
	 "time":"2019-11-06T11:17:00Z",
	 "myextension":"extension value"
}
ubuntu@i-a1kwg7lk:~/functions-framework-nodejs$
```

输入测试2

```sh
curl -X POST \
     -d'@./mock/payload/structured.json' \
     -H'Content-Type:application/cloudevents+json' \
     http://localhost:8080/
```

返回结果2

```sh
ubuntu@i-a1kwg7lk:~/functions-framework-nodejs$ curl -X POST \
>      -d'@./mock/payload/structured.json' \
>      -H'Content-Type:application/cloudevents+json' \
>      http://localhost:8080/
{
	"specversion":"1.0",
	"type":"com.github.pull.create",
	"source":"https://github.com/cloudevents/spec/pull/123",
	"id":"b25e2717-a470-45a0-8231-985a99aa9416",
	"time":"2019-11-06T11:08:00Z",
	"datacontenttype":"application/json",
	"data":{
		"framework":"openfunction"
	}
}
ubuntu@i-a1kwg7lk:~/functions-framework-nodejs$
```

##### openfunction 

在本小节中展示 openfunction 函数类型 demo

本 demo 中主要有三个文件，分别为 index.js 、package.json 、config.json

- index.js

  ```sh
  exports.helloWorld = (data) => {
    return('Hello, World!\n');
  };
  
  ```

- package.json

  ```sh
  {
    "dependencies": {
      "@openfunction/functions-framework": "0.3.0"
    },
    "scripts": {
      "start": "functions-framework --target=helloWorld --source openfunction "
    }
  }
  ```

###### 生成 app image

使用 builder of/node14 将用户源码构建为镜像 node-openfunction

```sh
pack build node-openfunction  --builder of/node14
```

###### 起容器测试

以镜像 node-openfunction 起容器测试

```sh
docker run --rm -p8080:8080 node-openfunction
```

打开另一个终端访问 ，可看到输出结果

```sh
ubuntu@i-a1kwg7lk:~$ curl localhost:8080
{"data":"Hello, World!\n"}
```

#### Q & A

**Q1:** ERROR: failed to add buildpacks to builder: downloading buildpack: extracting from registry 'bundle.tgz': fetching image: image 'bundle.tgz' does not exist on the daemon: not found
Extracting builder tar:

```shell
openfunction6@cloudshell:~/mph/builder$ bazel build //builders/ruby26:builder.image
INFO: Analyzed target //builders/ruby26:builder.image (1 packages loaded, 5 targets configured).
INFO: Found 1 target...
ERROR: /home/openfunction6/mph/builder/builders/ruby26/BUILD.bazel:7:8: Executing genrule //builders/ruby26:builder.image failed: (Exit 1): bash failed: error executing command /bin/bash -c ... (remaining 1 argument(s) skipped)
2021/08/04 09:09:31 Checking tools
2021/08/04 09:09:31 pack found at /usr/local/bin/pack
2021/08/04 09:09:31 docker found at /usr/bin/docker
2021/08/04 09:09:31 container-structure-test found at /usr/local/bin/container-structure-test
2021/08/04 09:09:31 Checking pack version
2021/08/04 09:09:31 Found pack version 0.20.0+git-66a4f32.build-2668
ERROR: failed to add buildpacks to builder: downloading buildpack: extracting from registry 'bundle.tgz': fetching image: image 'bundle.tgz' does not exist on the daemon: not found
Extracting builder tar:
./
./builder.toml
./label.tgz
./ruby/
./ruby/bundle.tgz
./ruby/functions_framework.tgz
Warning: Command 'pack create-builder' has been deprecated, please use 'pack builder create' instead
Downloading from 'https://github.com/buildpacks/lifecycle/releases/download/v0.11.1/lifecycle-v0.11.1+linux.x86-64.tgz'
5.25 MB/5.25 MB
Target //builders/ruby26:builder.image failed to build
Use --verbose_failures to see the command lines of failed build steps.
```

**A1:** 这是由于 builder.toml 里的路径设置不正确导致，正确路径为

```shell
[[buildpacks]]
  id = "google.ruby.bundle"
  uri = "ruby/bundle.tgz"
```

 **Q2:**  [builder] /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require': cannot load such file -- zlib (LoadError)

```shell
openfunction6@cloudshell:~/mph/samples/functions/Knative/hello-world-ruby$ pack build function-ruby1 --builder penghuima/ruby26  --env FUNC_NAME="Hello-World"
v1: Pulling from penghuima/buildpacks-ruby26-run
Digest: sha256:241dbbb3eefe5ef0a4ee5d471933ed331eb6a18d20cf5cdeffb6f9da6a44cc35
Status: Image is up to date for penghuima/buildpacks-ruby26-run:v1
0.11.1: Pulling from buildpacksio/lifecycle
Digest: sha256:2ca9870d9472400792bdc2d43ded8df2642ffcf937e8cd01685836678c28780b
[builder] Running "bundle lock --add-platform x86_64-linux"
[builder] /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require': cannot load such file -- zlib (LoadError)
[builder]       from /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/fetcher.rb:6:in `<top (required)>'
[builder]       from /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require'
[builder]       from /usr/local/lib/ruby/2.6.0/rubygems/core_ext/kernel_require.rb:54:in `require'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/cli/lock.rb:21:in `run'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/cli.rb:643:in `lock'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/vendor/thor/lib/thor/command.rb:27:in `run'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/vendor/thor/lib/thor/invocation.rb:126:in `invoke_command'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/vendor/thor/lib/thor.rb:387:in `dispatch'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/cli.rb:27:in `dispatch'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/vendor/thor/lib/thor/base.rb:466:in `start'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/cli.rb:18:in `start'
[builder]       from /usr/local/lib/ruby/gems/2.6.0/gems/bundler-1.17.2/exe/bundle:30:in `block in <top (required)>'
[builder]       from /usr/local/lib/ruby/2.6.0/bundler/friendly_errors.rb:124:in `with_friendly_errors'
[builder]       from /usr/local/lib/ruby/gems/2.6.0/gems/bundler-1.17.2/exe/bundle:22:in `<top (required)>'
[builder]       from /usr/local/bin/bundle:23:in `load'
[builder]       from /usr/local/bin/bundle:23:in `<main>'
[builder] Sorry your project couldn't be built.
[builder] Our documentation explains ways to configure Buildpacks to better recognise your project:
[builder]  -> https://github.com/GoogleCloudPlatform/buildpacks/blob/main/README.md
[builder] If you think you've found an issue, please report it:
[builder]  -> https://github.com/GoogleCloudPlatform/buildpacks/issues/new
[builder] --------------------------------------------------------------------------------
[builder] ERROR: failed to build: exit status 1
ERROR: failed to build: executing lifecycle. This may be the result of using an untrusted builder: failed with status code: 51
```

**A2：**这是由于 stack 运行时安装少依赖 zlib 导致的,安装ruby时缺少 openssl 和 zlib

```shell
*** Following extensions are not compiled:
dbm:
        Could not be configured. It will not be installed.
        Check ext/dbm/mkmf.log for more details.
gdbm:
        Could not be configured. It will not be installed.
        Check ext/gdbm/mkmf.log for more details.
openssl:
        Could not be configured. It will not be installed.
        /tmp/ruby/ruby-2.6.7/ext/openssl/extconf.rb:97: OpenSSL library could not be found. You might want to use --with-openssl-dir=<dir>                            option to specify the prefix where OpenSSL is installed.
        Check ext/openssl/mkmf.log for more details.
readline:
        Could not be configured. It will not be installed.
        /tmp/ruby/ruby-2.6.7/ext/readline/extconf.rb:62: Neither readline nor libedit was found
        Check ext/readline/mkmf.log for more details.
zlib:
        Could not be configured. It will not be installed.
        Check ext/zlib/mkmf.log for more details.
```

> 解决方案，[参考 1](https://blog.csdn.net/qq_41690477/article/details/82750530)  [参考2](https://blog.csdn.net/Wangdada111/article/details/73382554?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_title~default-1.control&spm=1001.2101.3001.4242)

```shell
# install ruby runtime
RUN cd /tmp && mkdir ruby && cd ruby && curl  https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.6.tar.gz | tar xz \
    && cd ruby-2.6.6 && ./configure && make && make install  \
    && cd ext/zlib \
    && ruby extconf.rb && sed -i 's?$(top_srcdir)?../..?g' Makefile  && make && make install \
    && cd ../openssl \
    && ruby extconf.rb && sed -i 's?$(top_srcdir)?../..?g' Makefile && make && make install \
    && gem uninstall bundle && gem install bundler -v 2.1.4 \
```

**Q3：**缺少 Gemfile.lock文件

**A3:**  补充文件，注意依赖版本问题

**Q4：**stack 里 build 和 run 镜像 bundler 版本不一致，一个为 2.1.4 ，一个为 1.17.2

**A4：** 卸载默认bundler版本，统一版本为2.1.4





